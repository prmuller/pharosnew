﻿<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
    <title>{{$profile->user->name}}</title>
</head>
<body style="width: 100%">
    <div style="margin-bottom:75px">
    <div>Nome: {{$profile->fancy_name}}  |  DRT: {{$profile->drt}}</div>
    <div>Altura: {{$profile->height}}  |  Camisa: {{$profile->shirt}}</div>
    
    <div>Manequim: {{$profile->dummy}}  |  Sapato: {{$profile->feet}}</div>
   
    <div>Aniversario: {{\Carbon\Carbon::parse($profile->date_birth)->format('d/m/Y')}}</div>
    <div>Cor dos olhos: {{$profile->eye_color}}  |  Cor do cabelo: {{$profile->hair_color}}</div>
  
      <h3>Fotos:</h3>
    </div>  
        
    <div style="width:100%; text-align: center;">
        
    @foreach ($profile->medias->sortByDesc('order')->slice(0, 3) as $image)  

        <img src="{{ public_path()}}/uploads/profiles/{{$profile->user_id}}/{{$image->path}}" style="width:30%; padding-left: 20px; margin-top: 5px;" alt="">
    @endforeach
    <img src="{{ public_path()}}/media/various/logo-pdf.png" style=" width:30%; padding-left:20px; "> 

    </div>


   
</body>
</html>


