let store = {
    state: {
        cart: [],
        cartCount: 0,
        filters: {},
        profiles: [],
        profileList: [],
    },

    mutations: {
        addToCart(state, item) {
            state.cart.push(item);
            state.cartCount++;
        },
        setProfiles(state, profiles) {
            state.profiles = state.profileList = profiles;
        },
        setFilters(state, filters) {
            state.filters = filters;
        },
    },
    actions: {
        getProfiles({ commit, state }) {
            axios.get('/api/profiles', { params: state.filters}).then(response => {
                commit('setProfiles', response.data);
            });
        },

        setFilters({ commit, dispatch }, filters) {
            commit('setFilters', filters)
        }
    }
};

export default store;
